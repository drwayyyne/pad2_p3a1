// pad2_praktikum3_aufgabe1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <iostream>


class Bas {
public:
	Bas();
	virtual ~Bas();
	Bas* next;
};

Bas::Bas() { std::cout << "BasConst" << std::endl; };
Bas::~Bas() { std::cout << "BasDes" << std::endl; }



class Pro : public Bas {
private:
	std::string name;
	int         value;
public:
	Pro();
	~Pro();
	void set(int);
	void increase();
	void print() const;
	static Pro *last;
};

Pro::Pro() :value{} { std::cout << "ProConst" << std::endl; last = this; std::cout << last << std::endl; }
Pro::~Pro() { std::cout << "ProDes" << std::endl; }
void Pro::set(int val) { value = val; }
void Pro::increase() { std::cout << ++value << std::endl; }
void Pro::print() const { std::cout << "Pro" << std::endl; }
Pro *Pro::last{ nullptr };



class Roo : public Bas {
private:
	std::string name;
	int         value;
public:
	Roo();
	~Roo();
	void set(int);
	void increase();
	void print() const;
};

Roo::Roo() {};
Roo::~Roo() {};
void Roo::set(int val) { value = val; }
void Roo::increase() { ++value; }
void Roo::print() const { std::cout << value << std::endl; }


int main()
{
	
	
	Bas *basObj = new Pro();

	std::cout << "Bas: " << basObj << std::endl;


	delete basObj;
	
	//Pro *proObj0 = new Pro();
	//Pro *proObj1 = new Pro();
	//Pro *proObj2 = new Pro();
	//Pro *proObj3 = new Pro();
	//Pro *proObj4 = new Pro();

	//Roo *rooObj0 = new Roo();
	//Roo *rooObj1 = new Roo();
	//Roo *rooObj2 = new Roo();
	//Roo *rooObj3 = new Roo();
	//Roo *rooObj4 = new Roo();

	//

	//delete proObj0;
	//delete proObj1;
	//delete proObj2;
	//delete proObj3;
	//delete proObj4;

	//delete rooObj0;
	//delete rooObj1;
	//delete rooObj2;
	//delete rooObj3;
	//delete rooObj4;


	/*
	Pro asshole = new Pro();
	Pro fuckhead = new Pro();
	Pro dickshit = new Pro();
	Pro cuntfuck = new Pro();
	Pro asscunt = new Pro();

	Roo dickface = new Roo();
	Roo fuckface = new Roo();
	Roo skroch = new Roo();
	Roo shitcunt = new Roo();
	Roo shitbugger = new Roo();
	*/
}